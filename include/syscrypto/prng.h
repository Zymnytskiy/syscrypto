#ifndef SYSCRYPTO_INCLUDES_RANDOM_H
#define SYSCRYPTO_INCLUDES_RANDOM_H

#include <stdint.h>

typedef struct {
	int type;
} prng_t;

enum {
	PRNG_GENERATOR_MT = 1,
};

#define PRNG_GENERATOR_DEFAULT PRNG_GENERATOR_MT

prng_t*	prng_new(int generator_type, int seed);
prng_t*	prng_new_seeded(int generator_type);
uint32_t prng_rand(prng_t* prng);
void prng_random_bytes(prng_t*, uint8_t* output, uint32_t bytes);
void prng_delete(prng_t* prng);

#endif // SYSCRYPTO_INCLUDES_RANDOM_H

