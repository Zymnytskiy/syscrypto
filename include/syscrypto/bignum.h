#ifndef SYSCRYPTO_INCLUDE_BIGNUM_H
#define SYSCRYPTO_INCLUDE_BIGNUM_H

#include <stdint.h>

typedef struct {
	uint32_t* places;
	uint32_t len;
	int minus;
} bignum_t;

bignum_t* bignum_new(void);
bignum_t* bignum_from_str(const char* str, int base);
bignum_t* bignum_from_uint(unsigned int i);
bignum_t* bignum_from_bytes(uint8_t* bytes, uint32_t num);
char* bignum_to_str(bignum_t* bn, int base);
bignum_t* bignum_add(bignum_t* left, bignum_t* right);
bignum_t* bignum_sub(bignum_t* left, bignum_t* right);
bignum_t* bignum_mul(bignum_t* left, bignum_t* right);
int bignum_div(bignum_t** q, bignum_t** r, bignum_t* left, bignum_t* right);
int bignum_cmp(bignum_t* a, bignum_t* b);
bignum_t* bignum_mod_exp(bignum_t* b, bignum_t* e, bignum_t* m);
int bignum_is_prime(bignum_t* n);
bignum_t* bignum_gcd(bignum_t* a, bignum_t* b);
bignum_t* bignum_euclidean_extended(bignum_t** r, bignum_t** s, bignum_t** t, bignum_t* a, bignum_t* b);
void bignum_delete(bignum_t* b);

#endif // SYSCRYPTO_INCLUDE_BIGNUM_H

