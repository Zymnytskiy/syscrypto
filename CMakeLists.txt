cmake_minimum_required(VERSION 3.5)

include_directories(${CMAKE_SOURCE_DIR}/include ${CMAKE_SOURCE_DIR}/src)

add_library(syscrypto src/bignum.c src/prng.c src/prng/mersenne_twister.c)

add_subdirectory(test)

enable_testing()

file(GLOB tests ${PROJECT_BINARY_DIR}/test/*_test)

foreach(test ${tests})
	add_test(${test} ${test})
endforeach()

