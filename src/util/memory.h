#ifndef SYSCRYPTO_SRC_UTIL_MEMORY_H
#define SYSCRYPTO_SRC_UTIL_MEMORY_H

#include <stdlib.h>

#define ALLOC_STRUCT(type) ((type*)malloc(sizeof(type)))
#define ALLOC_STRUCTS(type,num) ((type*)malloc(sizeof(type)*(num)))

#define REALLOC_STRUCTS(type,num,data) (data = (type*)realloc(data, sizeof(type)*num))

#endif // SYSCRYPTO_SRC_UTIL_MEMORY_H

