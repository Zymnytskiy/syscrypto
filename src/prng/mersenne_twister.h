#ifndef SYSCRYPTO_SRC_PRNG_MERSENNE_TWISTER_H
#define SYSCRYPTO_SRC_PRNG_MERSENNE_TWISTER_H

#include <syscrypto/prng.h>

prng_t* prng_mt_new(int seed);
uint32_t prng_mt_rand(prng_t* prng);
void prng_mt_delete(prng_t* prng);

#endif

