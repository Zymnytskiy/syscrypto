#include <syscrypto/prng.h>
#include <syscrypto/bignum.h>

#include "test.h"

void
prng_rand_seed_zero_test(void)
{
	prng_t* prng = prng_new(PRNG_GENERATOR_DEFAULT, 0);

	TEST_EQUAL(prng_rand(prng), 2357136044, "1st random number");
	TEST_EQUAL(prng_rand(prng), 2546248239, "2nd random number");
	TEST_EQUAL(prng_rand(prng), 3071714933, "3rd random number");
	TEST_EQUAL(prng_rand(prng), 3626093760, "4th random number");
	TEST_EQUAL(prng_rand(prng), 2588848963, "5th random number");

	prng_delete(prng);
}

void
prng_random_bytes_test(void)
{
	prng_t* prng = prng_new(PRNG_GENERATOR_DEFAULT, 0);
	uint8_t bytes[17];

	prng_random_bytes(prng, bytes, 16);
	bytes[16] = 0;
	TEST_EQUAL_STRINGS(bytes, "\xac\x0a\x7f\x8c\x2f\xaa\xc4\x97\x75\xa6\x16\xb7\xc0\xcc\x21\xd8", "prng_random_bytes");

	prng_delete(prng);
}

void
prng_random_bytes_test2(void)
{
	prng_t* prng = prng_new(PRNG_GENERATOR_DEFAULT, 0);
	uint8_t bytes[18];

	prng_random_bytes(prng, bytes, 17);
	bytes[17] = 0;
	TEST_EQUAL_STRINGS(bytes, "\xac\x0a\x7f\x8c\x2f\xaa\xc4\x97\x75\xa6\x16\xb7\xc0\xcc\x21\xd8\x43", "prng_random_bytes");

	prng_delete(prng);
}

int
main(void)
{
	prng_rand_seed_zero_test();
	prng_random_bytes_test();
	prng_random_bytes_test2();

	return 0;
}

