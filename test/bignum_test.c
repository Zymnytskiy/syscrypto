#include <syscrypto/bignum.h>

#include "test.h"

void
bignum_to_str_test(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_uint(2);
	bignum_t* bn3 = bignum_from_str("-123456754321", 10);
	bignum_t* zero = bignum_from_str("0", 10);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(bn, 10), "374144419156711147060143317175368453031918731001855", "bignum_to_str");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(bn2, 10), "2", "bignum_to_str");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(bn3, 10), "-123456754321", "bignum_to_str");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(zero, 10), "0", "bignum_to_str");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(bn3);
}

void
bignum_add_test(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_uint(2);
	bignum_t* result = bignum_add(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "374144419156711147060143317175368453031918731001857", "bignum_add");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_add_test2(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_uint(1);
	bignum_t* result = bignum_add(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 16), "100000000", "bignum_add (+,+)");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_add_test3(void)
{
	bignum_t* bn = bignum_from_str("-FFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_str("-1", 10);
	bignum_t* result = bignum_add(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 16), "-100000000", "bignum_add (-,-)");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_add_test4(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_str("-1", 10);
	bignum_t* result = bignum_add(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 16), "fffffffe", "bignum_add (+,-)");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_add_test5(void)
{
	bignum_t* bn = bignum_from_str("-FFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_str("1", 10);
	bignum_t* result = bignum_add(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 16), "-fffffffe", "bignum_add (-,+)");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_sub_test(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_uint(2);
	bignum_t* result = bignum_sub(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "374144419156711147060143317175368453031918731001853", "bignum_sub");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_sub_test2(void)
{
	bignum_t* bn = bignum_from_str("2", 10);
	bignum_t* bn2 = bignum_from_uint(3);
	bignum_t* result = bignum_sub(bn, bn2);

	TEST_EQUAL(result->minus, 1, "has minus");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "-1", "bignum_sub");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_mul_test(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_uint(2);
	bignum_t* result = bignum_mul(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "748288838313422294120286634350736906063837462003710", "bignum_mul");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_mul_test2(void)
{
	bignum_t* bn = bignum_from_str("52744dd", 16);
	bignum_t* bn2 = bignum_from_str("52744dd", 16);
	bignum_t* result = bignum_mul(bn, bn2);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 16), "1a8eb6afdc26c9", "bignum_mul");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(result);
}

void
bignum_div_test(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFFF", 16);
	bignum_t* bn2 = bignum_from_uint(0x80);

	bignum_t *q, *r;
	int err = bignum_div(&q, &r, bn, bn2);

	TEST_EQUAL(err, 0, "bignum_div");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(q, 10), "536870911", "bignum_div");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(r, 10), "127", "bignum_div");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(q);
	bignum_delete(r);
}

void
bignum_div_test2(void)
{
	bignum_t* bn = bignum_from_str("FFFFFFFFFFFFF1", 16);
	bignum_t* bn2 = bignum_from_str("FFFFFFFFFFF", 16);

	bignum_t *q, *r;
	int err = bignum_div(&q, &r, bn, bn2);

	TEST_EQUAL(err, 0, "bignum_div");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(q, 10), "4096", "bignum_div");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(r, 10), "4081", "bignum_div");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(q);
	bignum_delete(r);
}

void
bignum_div_test3(void)
{
	bignum_t* bn = bignum_from_str("100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 16);
	bignum_t* bn2 = bignum_from_str("3c65794943e24f816d65c37a2a918e594b7b197869cf7c9c67eb5d8abd8a4ef0ccf9ff1a31f67b9a50b3bcd13bd91889f14962fed78859b2f08051ed2b730cc5", 16);

	bignum_t *q, *r;
	int err = bignum_div(&q, &r, bn, bn2);

	TEST_EQUAL(err, 0, "bignum_div");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(q, 10), "4", "bignum_div q");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(r, 10), "754947170777878774503251469933268772429516651776986395645061140682893799884480757261101661184472990677950681054957584621327038962418784532454491563478252", "bignum_div r");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(q);
	bignum_delete(r);
}

void
bignum_div_test4(void)
{
	bignum_t* bn = bignum_from_str("100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000", 16);
	bignum_t* bn2 = bignum_from_str("c13489298dd9179b86d2de8c955b801adc81d35e33310d5ee6a3feb8e8464d1e70af8d4c58ee6207a662f38a1cec1bb7de155b6371c80c0e99fab25658b40283", 16);

	bignum_t *q, *r;
	int err = bignum_div(&q, &r, bn, bn2);

	TEST_EQUAL(err, 0, "bignum_div");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(q, 10), "1", "bignum_div q");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(r, 16), "3ecb76d67226e864792d21736aa47fe5237e2ca1cccef2a1195c014717b9b2e18f5072b3a7119df8599d0c75e313e44821eaa49c8e37f3f166054da9a74bfd7d", "bignum_div r");

	bignum_delete(bn);
	bignum_delete(bn2);
	bignum_delete(q);
	bignum_delete(r);
}

void
bignum_cmp_test(void)
{
	bignum_t* a = bignum_from_str("1234567890987654321", 10);
	bignum_t* b = bignum_from_str("1234567890987654320", 10);

	TEST_EQUAL(bignum_cmp(a, b), 1, "a > b");
	TEST_EQUAL(bignum_cmp(b, a), -1, "b < a");
	TEST_EQUAL(bignum_cmp(a, a), 0, "a == a");

	bignum_delete(a);
	bignum_delete(b);
}

void
bignum_from_bytes_test(void)
{
	bignum_t* bn = bignum_from_bytes("\xFF\x12\x56\xA0\x12\x57\x12\x97\x46\x12\x09\x24", 12);
	bignum_t* bn2 = bignum_from_bytes("\xFF\x12\x56\xA0\x12\x57\x12\x97\x46\x12\x09", 11);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(bn, 16), "2409124697125712A05612FF", "bignum_from_bytes");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(bn2, 16), "9124697125712A05612FF", "bignum_from_bytes");

	bignum_delete(bn);
	bignum_delete(bn2);
}

void
bignum_mod_exp_test(void)
{
	bignum_t* b = bignum_from_uint(2);
	bignum_t* e = bignum_from_uint(56);
	bignum_t* m = bignum_from_uint(23);
	bignum_t* result = bignum_mod_exp(b, e, m);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "2", "bignum_mod_exp");

	bignum_delete(b);
	bignum_delete(e);
	bignum_delete(m);
	bignum_delete(result);
}

void
bignum_mod_exp_test2(void)
{
	bignum_t* b = bignum_from_str("fffffffffffffffff", 16);
	bignum_t* e = bignum_from_str("5858", 16);
	bignum_t* m = bignum_from_str("12378953", 16);
	bignum_t* result = bignum_mod_exp(b, e, m);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "190591039", "bignum_mod_exp");

	bignum_delete(b);
	bignum_delete(e);
	bignum_delete(m);
	bignum_delete(result);
}

void
bignum_mod_exp_test3(void)
{
	bignum_t* b = bignum_from_str("2", 10);
	bignum_t* e = bignum_from_str("3163215189791179581267693382068144338762462292203851745519625075759717557547266554885193159245607609253020294282882116558106710962381946353494789360651460", 10);
	bignum_t* m = bignum_from_str("3163215189791179581267693382068144338762462292203851745519625075759717557547266554885193159245607609253020294282882116558106710962381946353494789360651461", 10);
	bignum_t* result = bignum_mod_exp(b, e, m);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(result, 10), "1", "bignum_mod_exp");

	bignum_delete(b);
	bignum_delete(e);
	bignum_delete(m);
	bignum_delete(result);
}

void
bignum_is_prime_test(void)
{
	bignum_t* n1 = bignum_from_str("3163215189791179581267693382068144338762462292203851745519625075759717557547266554885193159245607609253020294282882116558106710962381946353494789360651461", 10);
	bignum_t* n2 = bignum_from_str("3163215189791179581267693382068144338762462292203851745519625075759717557547266554885193159245607609253020294282882116558106710962381946353494789360651462", 10);
	bignum_t* n3 = bignum_from_str("10118978332345537362783317301485306566329975101352065226338034677419302515010249986444833513342014009707874626156357689677998275727380824050888577119421059", 10);

	TEST_EQUAL(bignum_is_prime(n1), 1, "bignum_is_prime 1");
	TEST_EQUAL(bignum_is_prime(n2), 0, "bignum_is_prime 2");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(n3, 16), "c13489298dd9179b86d2de8c955b801adc81d35e33310d5ee6a3feb8e8464d1e70af8d4c58ee6207a662f38a1cec1bb7de155b6371c80c0e99fab25658b40283", "bignum_to_str 3");
	TEST_EQUAL(bignum_is_prime(n3), 1, "bignum_is_prime 3");

	bignum_delete(n1);
	bignum_delete(n2);
	bignum_delete(n3);
}

void
bignum_gcd_test(void)
{
	bignum_t* a = bignum_from_str("3163215189791179581267693382068144338762462292203851745519625075759717557547266554885193159245607609253020294282882116558106710962381946353494789360651461", 10);
	bignum_t* b = bignum_from_str("3163215189791179581267693382068144338762462292203851745519625075759717557547266554885193159245607609253020294282882116558106710962381946353494789360651462", 10);
	bignum_t* gcd = bignum_gcd(a, b);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(gcd, 10), "1", "bignum_gcd");

	bignum_delete(a);
	bignum_delete(b);
	bignum_delete(gcd);
}

void
bignum_euclidean_extended_test(void)
{
	bignum_t* a = bignum_from_str("120", 10);
	bignum_t* b = bignum_from_str("23", 10);
	bignum_t *gcd, *s, *t;
	bignum_euclidean_extended(&gcd, &s, &t, a, b);

	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(gcd, 10), "1", "bignum_euclidean_extended");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(s, 10), "-9", "bignum_euclidean_extended");
	TEST_EQUAL_STRINGS_AND_FREE_FIRST(bignum_to_str(t, 10), "47", "bignum_euclidean_extended");

	bignum_delete(a);
	bignum_delete(b);
	bignum_delete(gcd);
	bignum_delete(s);
	bignum_delete(t);
}

int
main(void)
{
	bignum_to_str_test();
	bignum_add_test();
	bignum_add_test2();
	bignum_add_test3();
	bignum_add_test4();
	bignum_add_test5();
	bignum_sub_test();
	bignum_sub_test2();
	bignum_mul_test();
	bignum_mul_test2();
	bignum_div_test();
	bignum_div_test2();
	bignum_div_test3();
	bignum_mod_exp_test();
	bignum_mod_exp_test2();
	bignum_mod_exp_test3();
	bignum_div_test4();
	bignum_is_prime_test();
	bignum_gcd_test();
	bignum_euclidean_extended_test();

	return 0;
}

